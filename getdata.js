
/* Get form data  */
console.log("getting data by running our script");

  window.addEventListener("load", function () {
    var form1, form2, form3, form4, form5, form6;
    form1 = document.getElementById("form1");
    form2 = document.getElementById("form2");
    form3 = document.getElementById("form3");
    form4 = document.getElementById("form4");
    form5 = document.getElementById("form5");
    form6 = document.getElementById("form6");

    // form.addEventListener("submit", getdata);
    let fname = form1.elements[0];
    let lname = form1.elements[1];
    // document.getElementById("myBtn").addEventListener("click", getdata);

    document.getElementById("Next1").addEventListener("click", que1);
    document.getElementById("Next2").addEventListener("click", que2);
    document.getElementById("Next3").addEventListener("click", que3);
    document.getElementById("Next4").addEventListener("click", que4);
    document.getElementById("Next5").addEventListener("click", que5);
    document.getElementById("myBtn").addEventListener("click", que6);

    function que1() {
      console.log("Q1", localStorage.getItem("q1"));
    }
    function que2() {
      console.log("Q2", localStorage.getItem("q2"));
    }
    function que3() {
      console.log("Q3", localStorage.getItem("q3"));
    }
    function que4() {
      console.log("Q4", localStorage.getItem("q4"));
    }
    function que5() {
      console.log("Range", localStorage.getItem("range"));
    }
    function que6() {
      console.log("First name", localStorage.getItem("fname"));
      console.log("Email", localStorage.getItem("email"));
      console.log("Mobile number", localStorage.getItem("phone"));
    }

    function getdata() {
      console.log("Pop up form data");
      console.log("First name", fname);
      console.log("Last name", lname);
      console.log("Q1", localStorage.getItem("q1"));
      console.log("Q2", localStorage.getItem("q2"));
      console.log("Q3", localStorage.getItem("q3"));
      console.log("Q4", localStorage.getItem("q4"));
      console.log("First name", localStorage.getItem("fname"));
      console.log("Range", localStorage.getItem("range"));
      console.log("Email", localStorage.getItem("email"));
      console.log("Mobile number", localStorage.getItem("phone"));
    }

    // browser info
    console.log("Cookie Enabled " + navigator.cookieEnabled);
    console.log("App name is" + navigator.appName);
    console.log("AppCodeName is " + navigator.appCodeName);
    console.log("Product/Browser Engine is ", navigator.product);
    console.log("Browser version", navigator.appVersion);
    console.log("IS Java Enabled ?", navigator.javaEnabled());
    console.log(navigator.userAgent);
  });


     /* get os name  */
     function getOS() {
        var userAgent = window.navigator.userAgent,
          platform = window.navigator.platform,
          macosPlatforms = ["Macintosh", "MacIntel", "MacPPC", "Mac68K"],
          windowsPlatforms = ["Win32", "Win64", "Windows", "WinCE"],
          iosPlatforms = ["iPhone", "iPad", "iPod"],
          os = null;
    
        if (macosPlatforms.indexOf(platform) !== -1) {
          os = "Mac OS";
        } else if (iosPlatforms.indexOf(platform) !== -1) {
          os = "iOS";
        } else if (windowsPlatforms.indexOf(platform) !== -1) {
          os = "Windows";
        } else if (/Android/.test(userAgent)) {
          os = "Android";
        } else if (!os && /Linux/.test(platform)) {
          os = "Linux";
        }
    
        return os;
      }
    
      console.log("Operating system is ", getOS());
      /*  */
      /* Get Ip address */
    
       $.getJSON("https://api.ipify.org?format=json", 
                                          function(data) { 
             console.log("My IP address",data.ip); 
        });
        
        /* Get browser */
        
        var nVer = navigator.appVersion;
    var nAgt = navigator.userAgent;
    var browserName  = navigator.appName;
    var fullVersion  = ''+parseFloat(navigator.appVersion); 
    var majorVersion = parseInt(navigator.appVersion,10);
    var nameOffset,verOffset,ix;
    
    // In Opera 15+, the true version is after "OPR/" 
    if ((verOffset=nAgt.indexOf("OPR/"))!=-1) {
    browserName = "Opera";
    fullVersion = nAgt.substring(verOffset+4);
    }
    // In older Opera, the true version is after "Opera" or after "Version"
    else if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
    browserName = "Opera";
    fullVersion = nAgt.substring(verOffset+6);
    if ((verOffset=nAgt.indexOf("Version"))!=-1) 
    fullVersion = nAgt.substring(verOffset+8);
    }
    // In MSIE, the true version is after "MSIE" in userAgent
    else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
    browserName = "Microsoft Internet Explorer";
    fullVersion = nAgt.substring(verOffset+5);
    }
    // In Chrome, the true version is after "Chrome" 
    else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
    browserName = "Chrome";
    fullVersion = nAgt.substring(verOffset+7);
    }
    // In Safari, the true version is after "Safari" or after "Version" 
    else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
    browserName = "Safari";
    fullVersion = nAgt.substring(verOffset+7);
    if ((verOffset=nAgt.indexOf("Version"))!=-1) 
    fullVersion = nAgt.substring(verOffset+8);
    }
    // In Firefox, the true version is after "Firefox" 
    else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
    browserName = "Firefox";
    fullVersion = nAgt.substring(verOffset+8);
    }
    // In most other browsers, "name/version" is at the end of userAgent 
    else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
          (verOffset=nAgt.lastIndexOf('/')) ) 
    {
    browserName = nAgt.substring(nameOffset,verOffset);
    fullVersion = nAgt.substring(verOffset+1);
    if (browserName.toLowerCase()==browserName.toUpperCase()) {
    browserName = navigator.appName;
    }
    }
    // trim the fullVersion string at semicolon/space if present
    if ((ix=fullVersion.indexOf(";"))!=-1)
    fullVersion=fullVersion.substring(0,ix);
    if ((ix=fullVersion.indexOf(" "))!=-1)
    fullVersion=fullVersion.substring(0,ix);
    
    majorVersion = parseInt(''+fullVersion,10);
    if (isNaN(majorVersion)) {
    fullVersion  = ''+parseFloat(navigator.appVersion); 
    majorVersion = parseInt(navigator.appVersion,10);
    }
    
    console.log("Browser name is",browserName);




  
  
  
